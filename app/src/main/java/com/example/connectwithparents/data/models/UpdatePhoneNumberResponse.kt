package com.example.connectwithparents.data.models

import com.google.gson.annotations.SerializedName

data class UpdatePhoneNumberResponse (
    @SerializedName("phone")
    val phoneNumber: String?
)