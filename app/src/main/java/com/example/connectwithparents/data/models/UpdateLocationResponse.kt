package com.example.connectwithparents.data.models

import com.google.gson.annotations.SerializedName

class UpdateLocationResponse(
    @SerializedName("status")
    val status: Int?
)