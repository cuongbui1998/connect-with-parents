package com.example.connectwithparents.data.models

import com.google.gson.annotations.SerializedName

data class UpdateLocationParams(
    @SerializedName("id")
    val userId: String?,
    @SerializedName("latitude")
    val latitude: Double?,
    @SerializedName("longitude")
    val longitude: Double?,
    @SerializedName("locationName")
    val locationName: String?
)