package com.example.connectwithparents.data.models

import com.google.gson.annotations.SerializedName

data class GenerateCodeResponse(
    @SerializedName("code")
    val code: String?
)