package com.example.connectwithparents.data.models

import com.google.gson.annotations.SerializedName

data class LoginParam(
    @SerializedName("idToken")
    val idToken: String
)