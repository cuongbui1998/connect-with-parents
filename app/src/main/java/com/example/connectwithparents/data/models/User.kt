package com.example.connectwithparents.data.models

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("id")
    val id: String?,
    @SerializedName("name")
    val displayName: String?,
    @SerializedName("avatar")
    val imageUrl: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("phone")
    val phoneNumber: String?
)