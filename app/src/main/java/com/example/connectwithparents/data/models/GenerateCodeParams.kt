package com.example.connectwithparents.data.models

import com.google.gson.annotations.SerializedName

data class GenerateCodeParams(
    @SerializedName("id")
    val id: String
)