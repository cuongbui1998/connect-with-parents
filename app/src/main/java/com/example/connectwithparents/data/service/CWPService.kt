package com.example.connectwithparents.data.service

import com.example.connectwithparents.data.models.*
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.PUT

interface CWPService {
    @POST("api/kid/auth/google")
    fun login(@Body loginParam: LoginParam): Observable<User>

    @PUT("api/kid/phone")
    fun updatePhoneNumber(@Body updatePhoneNumberParams: UpdatePhoneNumberParams): Observable<UpdatePhoneNumberResponse>

    @PUT("api/kid/generate-code")
    fun generateCode(@Body generateCodeParams: GenerateCodeParams): Observable<GenerateCodeResponse>

    @PUT("api/kid/location")
    fun updateLocation(@Body updateLocationParams: UpdateLocationParams): Observable<UpdateLocationResponse>
}