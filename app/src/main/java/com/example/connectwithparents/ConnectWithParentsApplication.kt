package com.example.connectwithparents

import android.app.Application

class ConnectWithParentsApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        lateinit var instance: ConnectWithParentsApplication
    }

}