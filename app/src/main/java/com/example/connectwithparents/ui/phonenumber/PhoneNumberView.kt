package com.example.connectwithparents.ui.phonenumber

import com.example.connectwithparents.data.models.UpdatePhoneNumberResponse

interface PhoneNumberView {
    fun showNoNetworkConnection()

    fun showError(message: String?)

    fun onChangePhoneNumberSuccess(response: UpdatePhoneNumberResponse)
}