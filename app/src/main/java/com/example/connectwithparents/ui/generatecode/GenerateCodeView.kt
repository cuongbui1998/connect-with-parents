package com.example.connectwithparents.ui.generatecode

interface GenerateCodeView {
    fun showNoNetworkConnection()

    fun showError(message: String?)

    fun onSuccess(code: String)
}