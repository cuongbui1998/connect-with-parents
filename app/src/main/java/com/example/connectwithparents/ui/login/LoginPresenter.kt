package com.example.connectwithparents.ui.login

import com.example.connectwithparents.data.models.LoginParam
import com.example.connectwithparents.data.models.User
import com.example.connectwithparents.data.service.CWPService
import com.example.connectwithparents.data.service.RetrofitClient
import com.example.connectwithparents.ui.BasePresenter
import com.example.connectwithparents.utils.isNetworkError
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class LoginPresenter : BasePresenter<LoginView>() {
    private val cwpService: CWPService = RetrofitClient.fmkInstance.create(
        CWPService::class.java
    )

    fun loginWithGoogle(idToken: String) {
        val loginParam = LoginParam(idToken)
        cwpService.login(loginParam)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<User> {
                lateinit var disposable: Disposable
                override fun onComplete() {
                    disposable.dispose()
                }

                override fun onSubscribe(d: Disposable) {
                    disposable = d
                }

                override fun onNext(t: User) {
                    view?.onLoginSuccess(t)
                }

                override fun onError(e: Throwable) {
                    if(e.isNetworkError()){
                        view?.showNoNetworkConnection()
                    }else{
                        view?.showLoginError(e.message)
                    }
                }

            })
    }
}