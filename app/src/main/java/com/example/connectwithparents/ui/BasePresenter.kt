package com.example.connectwithparents.ui

open class BasePresenter<V> : Presenter<V>{

    protected var view: V? = null

    override fun attachView(view: V) {
        this.view = view
    }

    override fun detachView() {
        view = null
    }
}