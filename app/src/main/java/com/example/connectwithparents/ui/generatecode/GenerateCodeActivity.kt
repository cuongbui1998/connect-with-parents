package com.example.connectwithparents.ui.generatecode

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.connectwithparents.R
import com.example.connectwithparents.utils.showShortToast
import kotlinx.android.synthetic.main.activity_generate_code.*
import kotlinx.android.synthetic.main.my_toolbar.*

class GenerateCodeActivity : AppCompatActivity(), GenerateCodeView {

    private val generateCodePresenter = GenerateCodePresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_generate_code)

        init()
        initListener()
    }

    override fun onDestroy() {
        generateCodePresenter.detachView()
        super.onDestroy()
    }

    private fun initListener() {
        btnBack.setOnClickListener {
            finish()
        }
    }

    private fun init() {
        txtTitle.text = getString(R.string.generate_code)
        progress.visibility = View.VISIBLE
        generateCodePresenter.attachView(this)
        generateCodePresenter.generateCode()
    }

    companion object {
        fun intentFor(context: Context): Intent {
            return Intent(context, GenerateCodeActivity::class.java)
        }
    }

    override fun showNoNetworkConnection() {
        progress.visibility = View.GONE
        showShortToast(getString(R.string.no_network_connection))
    }

    override fun showError(message: String?) {
        progress.visibility = View.GONE
        message?.let {
            showShortToast(it)
        }
    }

    override fun onSuccess(code: String) {
        progress.visibility = View.GONE
        txtCode.text = code
    }
}
