package com.example.connectwithparents.ui.phonenumber

import com.example.connectwithparents.data.models.UpdatePhoneNumberParams
import com.example.connectwithparents.data.models.UpdatePhoneNumberResponse
import com.example.connectwithparents.data.service.CWPService
import com.example.connectwithparents.data.service.RetrofitClient
import com.example.connectwithparents.ui.BasePresenter
import com.example.connectwithparents.utils.UserManager
import com.example.connectwithparents.utils.isNetworkError
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class PhoneNumberPresenter : BasePresenter<PhoneNumberView>() {

    private val cwpService: CWPService = RetrofitClient.fmkInstance.create(
        CWPService::class.java
    )

    fun updatePhoneNumber(phoneNumber: String) {
        UserManager.id?.let { id ->
            val param = UpdatePhoneNumberParams(id, phoneNumber)
            cwpService.updatePhoneNumber(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<UpdatePhoneNumberResponse> {
                    lateinit var disposable: Disposable
                    override fun onComplete() {
                        disposable.dispose()
                    }

                    override fun onSubscribe(d: Disposable) {
                        disposable = d
                    }

                    override fun onNext(t: UpdatePhoneNumberResponse) {
                        view?.onChangePhoneNumberSuccess(t)
                    }

                    override fun onError(e: Throwable) {
                        if (e.isNetworkError()) {
                            view?.showNoNetworkConnection()
                        } else {
                            view?.showError(e.message)
                        }
                    }

                })
        }
    }
}