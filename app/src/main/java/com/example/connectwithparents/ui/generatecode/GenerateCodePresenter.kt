package com.example.connectwithparents.ui.generatecode

import com.example.connectwithparents.data.models.GenerateCodeParams
import com.example.connectwithparents.data.models.GenerateCodeResponse
import com.example.connectwithparents.data.service.CWPService
import com.example.connectwithparents.data.service.RetrofitClient
import com.example.connectwithparents.ui.BasePresenter
import com.example.connectwithparents.utils.UserManager
import com.example.connectwithparents.utils.isNetworkError
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class GenerateCodePresenter : BasePresenter<GenerateCodeView>() {

    private val cwpService: CWPService = RetrofitClient.fmkInstance.create(
        CWPService::class.java
    )

    fun generateCode() {
        UserManager.id?.let { idUser ->
            val generateCodeParams = GenerateCodeParams(idUser)
            cwpService.generateCode(generateCodeParams)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<GenerateCodeResponse> {
                    lateinit var disposable: Disposable
                    override fun onComplete() {
                        disposable.dispose()
                    }

                    override fun onSubscribe(d: Disposable) {
                        disposable = d
                    }

                    override fun onNext(t: GenerateCodeResponse) {
                        t.code?.let { code ->
                            view?.onSuccess(code)
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e.isNetworkError()) {
                            view?.showNoNetworkConnection()
                        } else {
                            view?.showError(e.message)
                        }
                    }
                })
        }
    }
}