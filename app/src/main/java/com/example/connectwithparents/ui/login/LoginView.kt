package com.example.connectwithparents.ui.login

import com.example.connectwithparents.data.models.User

interface LoginView {

    fun showNoNetworkConnection()

    fun showLoginError(message: String?)

    fun onLoginSuccess(user: User)
}