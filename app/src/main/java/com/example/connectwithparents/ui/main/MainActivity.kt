package com.example.connectwithparents.ui.main

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.connectwithparents.R
import com.example.connectwithparents.service.LocationService
import com.example.connectwithparents.service.ServiceUtils
import com.example.connectwithparents.ui.generatecode.GenerateCodeActivity
import com.example.connectwithparents.ui.setting.SettingActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
        initListener()
    }

    private fun initListener() {
        btnGenerateCode.setOnClickListener {
            startActivity(GenerateCodeActivity.intentFor(this))
        }

        btnShareLocation.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                startService(Intent(this, LocationService::class.java).apply {
                    action = ServiceUtils.START_FOREGROUND_ACTION
                })
            } else {
                startService(Intent(this, LocationService::class.java).apply {
                    action = ServiceUtils.STOP_FOREGROUND_ACTION
                })
            }
        }

        btnSetting.setOnClickListener {
            startActivity(SettingActivity.intentFor(this))
        }
    }

    private fun initView() {
        val permissionAccessCoarseLocationApproved = ContextCompat
            .checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED

        if (permissionAccessCoarseLocationApproved) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                btnShareLocation.isEnabled = true
                btnShareLocation.isChecked =
                    ServiceUtils.checkServiceIsRunning(LocationService::class.java)
            } else {
                val backgroundLocationPermissionApproved = ContextCompat
                    .checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED

                if (backgroundLocationPermissionApproved) {
                    btnShareLocation.isEnabled = true
                    btnShareLocation.isChecked =
                        ServiceUtils.checkServiceIsRunning(LocationService::class.java)
                } else {
                    // App can only access location in the foreground. Display a dialog
                    // warning the user that your app must have all-the-time access to
                    // location in order to function properly. Then, request background
                    // location.
                    requestPermissions(
                        arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                        REQUEST_PERMISSION_CODE
                    )
                }
            }
        } else {
            // App doesn't have access to the device's location at all. Make full request
            // for permission.
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
                ),
                REQUEST_PERMISSION_CODE
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_PERMISSION_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    btnShareLocation.isEnabled = true
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    finish()
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    companion object {
        private const val REQUEST_PERMISSION_CODE = 1998

        fun intentFor(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }
}
