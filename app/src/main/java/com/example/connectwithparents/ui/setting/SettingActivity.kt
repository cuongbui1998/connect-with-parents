package com.example.connectwithparents.ui.setting

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.connectwithparents.R
import com.example.connectwithparents.service.LocationService
import com.example.connectwithparents.service.ServiceUtils
import com.example.connectwithparents.ui.phonenumber.PhoneNumberActivity
import com.example.connectwithparents.ui.splash.SplashActivity
import com.example.connectwithparents.utils.UserManager
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.my_toolbar.*

class SettingActivity : AppCompatActivity() {

    private lateinit var logoutManager: LogoutManager

    private val logoutResultCallback = object : LogoutManager.LogoutResultCallback {
        override fun onLogoutSuccess() {
            UserManager.deleteData()
            startActivity(SplashActivity.intentFor(this@SettingActivity))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        init()
        initListener()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                tvPhoneNumber.text = UserManager.phoneNumber
            }
        }
    }

    override fun onDestroy() {
        logoutManager.cleanUp()
        super.onDestroy()
    }

    private fun initListener() {
        btnBack.setOnClickListener {
            finish()
        }

        layoutPhoneNumber.setOnClickListener {
            startActivityForResult(
                PhoneNumberActivity.intentFor(
                    this,
                    PhoneNumberActivity.ACTION_UPDATE
                ),
                REQUEST_CODE
            )
        }

        layoutLogout.setOnClickListener {
            if (ServiceUtils.checkServiceIsRunning(LocationService::class.java)) {
                startService(Intent(this, LocationService::class.java).apply {
                    action = ServiceUtils.STOP_FOREGROUND_ACTION
                })
            }
            logoutManager.logoutGoogle()
        }
    }

    private fun init() {
        logoutManager = LogoutManager(this, logoutResultCallback)
        txtTitle.text = getString(R.string.setting)
        tvPhoneNumber.text = UserManager.phoneNumber
    }

    companion object {
        const val REQUEST_CODE = 1998

        fun intentFor(context: Context): Intent {
            return Intent(context, SettingActivity::class.java)
        }
    }
}
