package com.example.connectwithparents.ui

interface Presenter<V> {

    fun attachView(view: V)

    fun detachView()
}