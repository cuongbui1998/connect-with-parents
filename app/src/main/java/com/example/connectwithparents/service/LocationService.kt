package com.example.connectwithparents.service

import android.Manifest
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.*
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import com.example.connectwithparents.ConnectWithParentsApplication
import com.example.connectwithparents.R
import com.example.connectwithparents.data.models.UpdateLocationParams
import com.example.connectwithparents.data.models.UpdateLocationResponse
import com.example.connectwithparents.data.service.CWPService
import com.example.connectwithparents.data.service.RetrofitClient
import com.example.connectwithparents.utils.UserManager
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.util.*

class LocationService : Service() {

    private val serviceLocationListener = ServiceLocationListener(LocationManager.GPS_PROVIDER)

    private val locationManager: LocationManager =
        ConnectWithParentsApplication.instance.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    private val cwpService: CWPService = RetrofitClient.fmkInstance.create(
        CWPService::class.java
    )

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        handleAction(intent)
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private fun handleAction(intent: Intent?) {
        when (intent?.action) {
            ServiceUtils.START_FOREGROUND_ACTION -> {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return
                }
                locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    LOCATION_INTERVAL,
                    LOCATION_DISTANCE,
                    serviceLocationListener
                )
                startForeground(ServiceUtils.NOTIFICATION_ID, getNotification())
            }
            ServiceUtils.STOP_FOREGROUND_ACTION -> {
                stopForeground(true)
                stopSelf()
            }
        }
    }

    private fun getNotification(): Notification? {
        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel("my_service", "My Background Service")
            } else {
                // If earlier version channel ID is not used
                // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                ""
            }
        val builder = NotificationCompat.Builder(this, channelId)
            .setContentText(getString(R.string.notification_content))
            .setSmallIcon(R.mipmap.ic_launcher)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setAutoCancel(true)
        return builder.build()
    }

    private fun getAddress(lat: Double, lng: Double): String {
        val geocoder = Geocoder(ConnectWithParentsApplication.instance, Locale.getDefault())
        return try {
            val addresses: List<Address> = geocoder.getFromLocation(lat, lng, 1)
            val obj: Address = addresses[0]
            val add: String = obj.getAddressLine(0)
            Log.d("LocationService", "$lat $lng")
            Log.d("LocationService", "Address $add")
            add
        } catch (e: IOException) {
            // TODO
            ""
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(
            channelId,
            channelName, NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    inner class ServiceLocationListener(provider: String?) : LocationListener {
        private var lastLocation: Location? = null

        init {
            lastLocation = Location(provider)
        }

        override fun onLocationChanged(location: Location?) {
            lastLocation = location
            location?.let {
                val locationName = getAddress(it.latitude, it.longitude)
                val updateLocationParams =
                    UpdateLocationParams(UserManager.id, it.latitude, it.longitude, locationName)
                cwpService.updateLocation(updateLocationParams)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : Observer<UpdateLocationResponse> {
                        lateinit var disposable: Disposable
                        override fun onComplete() {
                            disposable.dispose()
                        }

                        override fun onSubscribe(d: Disposable) {
                            disposable = d
                        }

                        override fun onNext(t: UpdateLocationResponse) {
                            if (t.status == 1) {
                                Log.d("LocationService", "update success")
                            } else {
                                Log.d("LocationService", "update fail")
                            }
                        }

                        override fun onError(e: Throwable) {
                            Log.d("LocationService", e.toString())
                        }

                    })
            }
        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onProviderEnabled(provider: String?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onProviderDisabled(provider: String?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    }

    companion object {
        // minimum time interval between location updates in milliseconds
        const val LOCATION_INTERVAL: Long = 20L * 1000L

        //  minimum distance between location updates in meters
        const val LOCATION_DISTANCE = 10f
    }
}