package com.example.connectwithparents.service

import android.app.ActivityManager
import android.content.Context
import com.example.connectwithparents.ConnectWithParentsApplication

object ServiceUtils {
    const val NOTIFICATION_ID = 1234

    const val START_FOREGROUND_ACTION = "com.vng.muzic.service.action.startforeground"

    const val STOP_FOREGROUND_ACTION = "com.vng.muzic.service.action.stopforeground"

    fun checkServiceIsRunning(serviceClass: Class<*>): Boolean {
        (ConnectWithParentsApplication.instance.getSystemService(Context.ACTIVITY_SERVICE) as? ActivityManager)?.let { activityManager ->
            for (service in activityManager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.name == service.service.className) {
                    return true
                }
            }
            return false
        } ?: return false
    }
}